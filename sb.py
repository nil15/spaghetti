import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
import pandas as pd
from scipy.stats import variation
from statistics import mean
# https://gitlab.com/nil15/spaghetti/-/blob/master/sb.py

def end_slope(filenames,figcount,color,marker,label,sample):
    i = 0
    slope_list = []
    for f in filenames:
      xeq,yeq,b,n=fitting_slen_bod(f)
      slope = (yeq[-1] - yeq[-2])/(xeq[-1] - xeq[-2])
      slope_list.append(slope)
      plt.figure(figcount)
      plt.ylabel("b", fontsize=18)
      if i == 0:
        plt.plot(sample,slope,marker=marker,linestyle = "None", markersize=14,markerfacecolor="None",markeredgecolor=color,label=label)      
      else:  
        plt.plot(sample,slope,marker=marker,linestyle = "None", markersize=14,markerfacecolor="None",markeredgecolor=color)      
      plt.xticks([1,2,3])  
      i = i +1  
    return slope_list  

def calc_k(mean_slope):
    alpha = np.arctan(mean_slope)
    alpha = alpha/2.0
    return np.sin(alpha)


def slen_bod(x,b,n):
    return 1 - b * np.sin(n * np.pi * x)


def dat_extract(filename):
    s=pd.read_csv(filename)
    x=s["x"]
    y=s["y"]
    return x,y

def first_deriv(x,y,l):
    y_1d=np.zeros(l)
    for i in range(1,l-1):
        y_1d[i]=((y[i+1]-y[i-1])/(x[i+1]-x[i-1]))
    return y_1d

def second_deriv(x,y,l):
    y_2d=np.zeros(l)
    for i in range (1,l-1):
        y_2d[i]=((y[i+1]-2*y[i]+y[i-1])/(x[i+1]-x[i])**2)
    return y_2d

def rad_curv(y_1d,y_2d,l):
    r=np.zeros(l)
    for i in range(1,l-1):
        r[i]=abs(((1+(y_1d[i])**2)**(3.0/2.0))/y_2d[i])
    return r

def rad_curv_an(x,l,b,n):
    r=np.zeros(l)
    for i in range(l):
        num = (b**2 * np.pi**2 * n**2 * np.cos(n*np.pi*x[i])**2 + 1)**(3/2.0)
        r[i] = abs(b * np.pi**2 * n**2 * np.sin(n*np.pi*x[i]))

    return r

def fitting_slen_bod(filename):
    x,y = dat_extract(filename)
    fit,_ = curve_fit(slen_bod,x,y)
    b=fit[0]
    n=fit[1]
    #print('b,n',b,n)
    eval_y = []
    x_fine = np.linspace(0,max(x),100)
    for i in x_fine:
        eval_y.append(slen_bod(i,b,n))
    #print('size of x,y',len(x_fine),len(eval_y))    
    return np.asarray(x_fine)-0.475,eval_y,b,n

def force_calc(filenames,figcount,color,marker,label):
    f_list = []
    sample = []
    c = 0
    for f in filenames:
        x,y = dat_extract(f)
        xeq,yeq,b,n = fitting_slen_bod(f)
        SqrPx_EI = np.arcsin(yeq/b)
        SqrP_EI = SqrPx_EI/xeq
        d = 1.5e-3  #in m dia of spaghetti
        E = 5e9     #Youngs modulus in Pa or N/m^2
        I = np.pi * d**4/64.0
        P = SqrP_EI**2 * E * I  # Force
        f_list.append(P)
        sample.append(c)
        c = c + 1
    plt.figure(figcount)
    print("FORCE",f_list)
    plt.plot(sample, f_list, marker=marker,markeredgecolor=color,markeredgewidth=18,markerfacecolor="None",label=label)    

def slope_plot(filenames,figcount,color,marker, label):
  b_list = []
  n_list = []
  for f in filenames:
      x,y=dat_extract(f)
      xeq,yeq,b,n=fitting_slen_bod(f)
      b_list.append(b)
      n_list.append(n)
      #print('size of xeq,yeq',len(xeq),len(yeq))
      """
      slope_exp=[]
      slope_eq=[]
      x_exp=[]
      x_eq=[]
      
      for i in range(len(x)-1):
         xl=[x[i]-0.475,x[i+1]-0.475]
         yl=[y[i],y[i+1]]
         x_exp.append(x[i])
         slope_exp.append(slope_cat_exp(xl,yl))
      for i in range(len(xeq)-1):
         xeql=[xeq[i]-0.475,xeq[i+1]-0.475]
         yeql=[y[i],y[i+1]]
         x_eq.append(xeq[i])
         slope_eq.append(slope_cat_eq(xeql,yeql,a))
      
      """
      #plt.figure(figcount)
      #plt.title(f)
      #plt.plot(np.array(x_exp)-0.475,slope_exp,marker='o')
      #plt.plot(np.array(x_eq)-0.475,slope_eq)
      #figcount=figcount+1
      

      #plt.figure(figcount)
      
      #plt.title(f)
      #plt.plot(np.array(x)-0.475,y,marker='o')
      #plt.plot(np.array(xeq),yeq)
      
      #figcount=figcount+1
      """
      plt.figure(figcount)
      l = len(xeq)
      y1_d = first_deriv(np.asarray(xeq),yeq,l)
      y2_d = second_deriv(np.asarray(xeq),yeq,l)
      r = rad_curv(y1_d,y2_d,l)
      r = rad_curv_an(np.asarray(xeq),l,b,n)
      
      plt.plot(np.asarray(xeq),r,label='rad curv')
      plt.legend()
      """      


      #figcount=figcount+1
  plt.figure(figcount)    
  plt.title("Fitting coefficients", fontsize=18)
  plt.xlabel("coeff. b", fontsize=18)
  plt.ylabel("coeff. n", fontsize=18)
  av_b = sum(b_list)/len(b_list)
  av_n = sum(n_list)/len(n_list)
  plt.plot(b_list,n_list,marker=marker,linestyle = "None", markersize=14,markerfacecolor="None",markeredgecolor=color,label=label)    
  plt.plot(av_b,av_n,marker=marker,linestyle = "None", markersize=14,markerfacecolor=color,markeredgecolor=color,label="Average")
  return b_list


figcount=100

filenames = ["1_3t1.csv","2_3t1.csv","7_t1.csv","8_t1.csv","9_t1.csv"]
#force_calc(filenames,figcount+1,'r',"x","Wet 3min_test1")

filenames = filenames + ["3min_1t2.csv","3min_2t2.csv","3min_3t2.csv","3min_4t2.csv","3min_5t2.csv","3min_6t2.csv","3min_7t2.csv","3min_8t2.csv","3min_9t2.csv"]

#filenames = ["3min_1t2.csv","3min_2t2.csv","3min_3t2.csv","3min_4t2.csv","3min_5t2.csv"]
b_list_3min = slope_plot(filenames,figcount,'k',"o","Wet 3min All")
end_slope_list_3 = end_slope(filenames, figcount+1, 'k',"o", "slope 3min",1)




filenames = ["5min_2t1.csv","5min_3t1.csv","5min_4t1.csv","5min_5t1.csv","5min_6t1.csv"]
#slope_plot(filenames,figcount,'g',"x","Wet 5min_test1")


filenames = filenames + ["5min_1t2.csv","5min_2t2.csv","5min_3t2.csv","5min_4t2.csv","5min_5t2.csv","5min_6t2.csv","5min_7t2.csv","5min_8t2.csv","5min_9t2.csv"]

#filenames = ["5min_1t2.csv","5min_2t2.csv","5min_3t2.csv","5min_4t2.csv","5min_5t2.csv"]
b_list_5min = slope_plot(filenames,figcount,'k',"x","Wet 5min All")
end_slope_list_5 = end_slope(filenames, figcount+1, 'k',"x", "slope 5min",2)



filenames = ["1_a.csv","2_b.csv","3_b.csv","5_b.csv","6_b.csv","1_dry.csv","2_dry.csv","3_dry.csv","4_dry.csv","5_dry.csv","6_dry.csv","7_dry.csv","8_dry.csv","9_dry.csv","11_dry.csv","12_dry.csv","13_dry.csv"]
b_list_dry = slope_plot(filenames,figcount,'k',"*","Dry")
end_slope_list_dry = end_slope(filenames, figcount+1, 'k',"*", "slope Dry",3)



all_end_slopes = end_slope_list_dry + end_slope_list_3 + end_slope_list_5
mean_end_slopes = mean(all_end_slopes)

plt.figure(figcount+1)
plt.plot(1,variation(end_slope_list_3)+mean_end_slopes,marker = 'o',markerfacecolor='k',markeredgecolor = 'r', markersize=14)
plt.plot(2,variation(end_slope_list_5)+mean_end_slopes,marker = 'X',markerfacecolor='k', markeredgecolor = 'g', markersize=14)
plt.plot(3,variation(end_slope_list_dry)+mean_end_slopes,marker = '*',markerfacecolor='k', markeredgecolor = 'b', markersize=14)
frame1 = plt.gca()
frame1.axes.get_xaxis().set_visible(False)
#calculating the angle alpha

mean_slope_dry = mean(end_slope_list_dry) 
mean_slope_3min = mean(end_slope_list_3) 
mean_slope_5min = mean(end_slope_list_5) 

print("Mean Slope for dry, 3min, 5min   ")
print(mean_slope_dry," ", mean_slope_3min, " ", mean_slope_5min)
print("k for dry, 3min, 5min   ")
k_dry=calc_k(mean_slope_dry)
k_3min=calc_k(mean_slope_3min)
k_5min=calc_k(mean_slope_5min)
print(k_dry," ", k_3min, " ", k_5min)
print("Calculating max deflection 1-b")
h_dry = 1 - mean(b_list_dry)
h_3min = 1 - mean(b_list_3min)
h_5min = 1 - mean(b_list_5min)
print("h for dry, 3min, 5min   ")
print(h_dry," ", h_3min, " ", h_5min)
print("c for dry, 3min, 5min   ")
c_dry = h_dry/(2*k_dry)
c_3min = h_3min/(2*k_3min)
c_5min = h_5min/(2*k_5min)
print(h_dry/(2*k_dry)," ", h_3min/(2*k_3min), " ", h_5min/(2*k_5min))
print("Rmin for dry, 3min, 5min   ")
print(c_dry**2/h_dry," ", c_3min**2/h_3min, " ", c_5min**2/h_5min)




min5_t2_T = 79.5
min5_t1_T = 79.1
min3_t1_T = 61.8
min3_t2_T = 62.7
#plt.title("5t2_T = "+str(min5_t2_T)+",5t1_T = "+str(min5_t1_T)+",3t2_T = "+str(min3_t2_T)+",3t1_T = "+str(min3_t1_T))
plt.figure(figcount)
plt.legend()
plt.figure(figcount+1)
plt.legend()
plt.show()

    













