import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
import pandas as pd

def caten(x,a):
    return 0.5 * a * (np.exp(-(x-0.475)/a) + np.exp((x-0.475)/a))

def parab(x,a,b):
    return a*(x-0.475)**2+b

def slen_bod(x,b,n):
    return 1 - b * np.sin(n * np.pi * x)

def slope_cat_eq(x,y,a):
    return a*(np.cosh(x[1]/a)-np.cosh(x[0]/a))/(x[1]-x[0])

def slope_cat_exp(x,y):
    return (y[1]-y[0])/(x[1]-x[0])

def cubic_interp(x,y):
    #f=interp1d(x,y,kind='cubic')
    f=interp1d(x,y)
    return f

def dat_extract(filename):
    s=pd.read_csv(filename)
    x=s["x"]
    y=s["y"]
    return x,y

def fitting_parab(filename):
      print('filename',filename)
      x,y=dat_extract(filename)  
      fit,_ = curve_fit(parab,x,y)
      a=fit[0]
      b=fit[1]
      print('a,b',a, b)
      eval_y=[]
      for i in x:
        eval_y.append(parab(i, a, b))
      return x,eval_y,a, b 

def first_deriv(x,y,l):
    y_1d=np.zeros(l)
    for i in range(1,l-1):
        y_1d[i]=((y[i+1]-y[i-1])/(x[i+1]-x[i-1]))
    return y_1d

def second_deriv(x,y,l):
    y_2d=np.zeros(l)
    for i in range (1,l-1):
        y_2d[i]=((y[i+1]-2*y[i]+y[i-1])/(x[i+1]-x[i])**2)
    return y_2d

def rad_curv(y_1d,y_2d,l):
    r=np.zeros(l)
    for i in range(1,l-1):
        r[i]=abs(((1+(y_1d[i])**2)**(3.0/2.0))/y_2d[i])
    return r

def rad_curv_an(x,l,b,n):
    r=np.zeros(l)
    for i in range(l):
        num = (b**2 * np.pi**2 * n**2 * np.cos(n*np.pi*x[i])**2 + 1)**(3/2.0)
        r[i] = abs(b * np.pi**2 * n**2 * np.sin(n*np.pi*x[i]))

    return r

def fitting_slen_bod(filename):
    x,y = dat_extract(filename)
    fit,_ = curve_fit(slen_bod,x,y)
    b=fit[0]
    n=fit[1]
    #print('b,n',b,n)
    eval_y = []
    x_fine = np.linspace(0,max(x),100)
    for i in x_fine:
        eval_y.append(slen_bod(i,b,n))
    #print('size of x,y',len(x_fine),len(eval_y))    
    return np.asarray(x_fine)-0.475,eval_y,b,n

def force_calc(filenames,figcount,color,marker,label):
    f_list = []
    sample = []
    c = 0
    for f in filenames:
        x,y = dat_extract(f)
        xeq,yeq,b,n = fitting_slen_bod(f)
        SqrPx_EI = np.arcsin(yeq/b)
        SqrP_EI = SqrPx_EI/xeq
        d = 1.5e-3  #in m dia of spaghetti
        E = 5e9     #Youngs modulus in Pa or N/m^2
        I = np.pi * d**4/64.0
        P = SqrP_EI**2 * E * I  # Force
        f_list.append(P)
        sample.append(c)
        c = c + 1
    plt.figure(figcount)
    print("FORCE",f_list)
    plt.plot(sample, f_list, marker=marker,markeredgecolor=color,markeredgewidth=18,markerfacecolor="None",label=label)    

def slope_plot(filenames,figcount,color,marker, label):
  b_list = []
  n_list = []
  print(b_list,n_list)
  for f in filenames:
      print(f)
      x,y=dat_extract(f)
      xeq,yeq,b,n=fitting_slen_bod(f)
      b_list.append(b)
      n_list.append(n)
      #print('size of xeq,yeq',len(xeq),len(yeq))
      """
      slope_exp=[]
      slope_eq=[]
      x_exp=[]
      x_eq=[]
      
      for i in range(len(x)-1):
         xl=[x[i]-0.475,x[i+1]-0.475]
         yl=[y[i],y[i+1]]
         x_exp.append(x[i])
         slope_exp.append(slope_cat_exp(xl,yl))
      for i in range(len(xeq)-1):
         xeql=[xeq[i]-0.475,xeq[i+1]-0.475]
         yeql=[y[i],y[i+1]]
         x_eq.append(xeq[i])
         slope_eq.append(slope_cat_eq(xeql,yeql,a))
      
      """
      #plt.figure(figcount)
      #plt.title(f)
      #plt.plot(np.array(x_exp)-0.475,slope_exp,marker='o')
      #plt.plot(np.array(x_eq)-0.475,slope_eq)
      #figcount=figcount+1
      

      #plt.figure(figcount)
      
      #plt.title(f)
      #plt.plot(np.array(x)-0.475,y,marker='o')
      #plt.plot(np.array(xeq),yeq)
      
      #figcount=figcount+1
      """
      plt.figure(figcount)
      l = len(xeq)
      y1_d = first_deriv(np.asarray(xeq),yeq,l)
      y2_d = second_deriv(np.asarray(xeq),yeq,l)
      r = rad_curv(y1_d,y2_d,l)
      r = rad_curv_an(np.asarray(xeq),l,b,n)
      
      plt.plot(np.asarray(xeq),r,label='rad curv')
      plt.legend()
      """      


      #figcount=figcount+1
  plt.figure(figcount)    
  plt.title("Fitting coefficients")
  plt.xlabel("coeff. b", fontsize=18)
  plt.ylabel("coeff. n", fontsize=18)
  av_b = sum(b_list)/len(b_list)
  av_n = sum(n_list)/len(n_list)
  plt.plot(b_list,n_list,marker=marker,linestyle = "None", markersize=14,markerfacecolor="None",markeredgecolor=color,label=label)    
  plt.plot(av_b,av_n,marker=marker,linestyle = "None", markersize=14,markerfacecolor=color,markeredgecolor=color,label="Average")


figcount=100
print("Going to 3min t1")

filenames = ["1_3t1.csv","2_3t1.csv","7_t1.csv","8_t1.csv","9_t1.csv"]
#slope_plot(filenames,figcount,'r',"x","Wet 3min_test1")
#force_calc(filenames,figcount+1,'r',"x","Wet 3min_test1")

print("Going to 3min t2")

filenames = filenames + ["3min_1t2.csv","3min_2t2.csv","3min_3t2.csv","3min_4t2.csv","3min_5t2.csv","3min_6t2.csv","3min_7t2.csv","3min_8t2.csv","3min_9t2.csv"]

#filenames = ["3min_1t2.csv","3min_2t2.csv","3min_3t2.csv","3min_4t2.csv","3min_5t2.csv"]
slope_plot(filenames,figcount,'r',"o","Wet 3min All")

print("Going to 5min t1")

filenames = ["5min_2t1.csv","5min_3t1.csv","5min_4t1.csv","5min_5t1.csv","5min_6t1.csv"]
#slope_plot(filenames,figcount,'g',"x","Wet 5min_test1")

print("going to 5min t2")

filenames = filenames + ["5min_1t2.csv","5min_2t2.csv","5min_3t2.csv","5min_4t2.csv","5min_5t2.csv","5min_6t2.csv","5min_7t2.csv","5min_8t2.csv","5min_9t2.csv"]

#filenames = ["5min_1t2.csv","5min_2t2.csv","5min_3t2.csv","5min_4t2.csv","5min_5t2.csv"]
slope_plot(filenames,figcount,'g',"o","Wet 5min All")

filenames = ["1_a.csv","2_b.csv","3_b.csv","5_b.csv","6_b.csv","1_dry.csv","2_dry.csv","3_dry.csv","4_dry.csv","5_dry.csv","6_dry.csv","7_dry.csv","8_dry.csv","9_dry.csv","11_dry.csv","12_dry.csv","13_dry.csv"]
slope_plot(filenames,figcount,'b',"o","Dry")


min5_t2_T = 79.5
min5_t1_T = 79.1
min3_t1_T = 61.8
min3_t2_T = 62.7
plt.title("5t2_T="+str(min5_t2_T)+",5t1_T="+str(min5_t1_T)+",3t2_T="+str(min3_t2_T)+",3t1_T="+str(min3_t1_T))
plt.legend()
plt.show()

    













